use std::collections::HashMap;

use proc_macro::TokenStream;
use syn::{ parse_quote, visit };
use syn::visit::{ Visit };

#[proc_macro_derive(CustomDebug, attributes(debug))]
pub fn derive(input: TokenStream) -> TokenStream {
    let st = syn::parse_macro_input!(input as syn::DeriveInput);
    match do_expand(&st) {
        Ok(token) => token.into(),
        Err(e) => e.to_compile_error().into(),
    }
}

fn do_expand(st: &syn::DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let res = generate_debug_trait(st)?;
    eprintln!("{:#?}", res);
    return Ok(res);
}

fn generate_debug_core(
    st: &syn::DeriveInput
) -> syn::Result<(proc_macro2::TokenStream, Vec<String>, Vec<String>)> {
    let fields = get_fields_from_derive_input(st)?;
    let struct_name_ident = &st.ident;
    let struct_name_literal = &struct_name_ident.to_string();
    let mut fmt_body_stream = proc_macro2::TokenStream::new();
    fmt_body_stream.extend(
        quote::quote!(
        // 注意这里引用的是一个字符串，不是一个 syn::ident， 生成的代码会以字面量形式表现出来
                fmt.debug_struct(#struct_name_literal)
    )
    );

    let mut field_type_names = Vec::new();
    let mut phantomdata_type_param_names = Vec::new();

    for field in fields {
        let field_name_ident = field.ident.as_ref().unwrap();
        let field_name_literal = field_name_ident.to_string();
        let mut format_str = "{:?}".to_string();
        if let Some(format_custom) = get_custom_format_of_field(field)? {
            format_str = format_custom;
        }

        if let Some(s) = get_field_type_name(field)? {
            field_type_names.push(s);
        }

        if let Some(s) = get_phantomdate_generic_type_name(field)? {
            phantomdata_type_param_names.push(s);
        }

        fmt_body_stream.extend(
            quote::quote!(
            .field(#field_name_literal, &format_args!(#format_str, &self.#field_name_ident))
         )
        );
    }
    fmt_body_stream.extend(quote::quote!(.finish()));
    Ok((fmt_body_stream, field_type_names, phantomdata_type_param_names))
}

fn generate_debug_trait(st: &syn::DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let struct_name_ident = &st.ident;

    let (fmt_body_stream, field_type_names, phantomdata_type_param_names) =
        generate_debug_core(st)?;
    let mut generics_param_to_modify = st.generics.clone();
    let associated_types_map = get_generic_associated_types(st);
    
    if let Some(hatch) = get_struct_escape_hatch(st) {
        generics_param_to_modify.make_where_clause();
        generics_param_to_modify.where_clause
            .as_mut()
            .unwrap()
            .predicates.push(syn::parse_str(hatch.clone().as_str()).unwrap());
    } else {
        for g in generics_param_to_modify.params.iter_mut() {
            if let syn::GenericParam::Type(t) = g {
                let type_param_name = t.ident.to_string();
                if
                    phantomdata_type_param_names.contains(&type_param_name) &&
                    !field_type_names.contains(&type_param_name)
                {
                    continue;
                }
                // 下面三行代码，如果是关联类型， 就不要对反省参数'T'
                if
                    associated_types_map.contains_key(&type_param_name) &&
                    !field_type_names.contains(&type_param_name)
                {
                    continue;
                }
    
                t.bounds.push(parse_quote!(std::fmt::Debug));
            }
        }
    
        // 以下6行是第七关新加的， 关联类型的约束要放到where子句里
        generics_param_to_modify.make_where_clause();
        for (.., associated_types) in associated_types_map {
            for associated_type in associated_types {
                generics_param_to_modify.where_clause
                    .as_mut()
                    .unwrap()
                    .predicates.push(parse_quote!(#associated_type:std::fmt::Debug));
            }
        }
    }

    let (impl_generics, type_generices, where_clause) = generics_param_to_modify.split_for_impl();

    let res_stream =
        quote::quote!(
       impl #impl_generics std::fmt::Debug for #struct_name_ident  #type_generices #where_clause{
          fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
              #fmt_body_stream
          }
       }
    );
    Ok(res_stream)
}

type StructFields = syn::punctuated::Punctuated<syn::Field, syn::Token!(,)>;

fn get_fields_from_derive_input(st: &syn::DeriveInput) -> syn::Result<&StructFields> {
    if
        let syn::Data::Struct(
            syn::DataStruct { fields: syn::Fields::Named(syn::FieldsNamed { ref named, .. }), .. },
        ) = st.data
    {
        return Ok(named);
    }
    Err(syn::Error::new_spanned(st, "Must define on a struct, not Enum"))
}

fn get_custom_format_of_field(field: &syn::Field) -> syn::Result<Option<String>> {
    for attr in &field.attrs {
        if
            let Ok(syn::Meta::NameValue(syn::MetaNameValue { ref path, ref lit, .. })) =
                attr.parse_meta()
        {
            if path.is_ident("debug") {
                if let syn::Lit::Str(ref lit_str) = lit {
                    return Ok(Some(lit_str.value()));
                }
            }
        }
    }
    Ok(None)
}

fn get_phantomdate_generic_type_name(field: &syn::Field) -> syn::Result<Option<String>> {
    if let syn::Type::Path(syn::TypePath { path: syn::Path { ref segments, .. }, .. }) = field.ty {
        if let Some(syn::PathSegment { ref ident, ref arguments }) = segments.last() {
            if ident == "PhantomData" {
                if
                    let syn::PathArguments::AngleBracketed(
                        syn::AngleBracketedGenericArguments { ref args, .. },
                    ) = arguments
                {
                    if let Some(syn::GenericArgument::Type(syn::Type::Path(ref gp))) = args.first() {
                        if let Some(generic_ident) = gp.path.segments.first() {
                            return Ok(Some(generic_ident.ident.to_string()));
                        }
                    }
                }
            }
        }
    }
    Ok(None)
}

fn get_field_type_name(field: &syn::Field) -> syn::Result<Option<String>> {
    if let syn::Type::Path(syn::TypePath { path: syn::Path { ref segments, .. }, .. }) = field.ty {
        if let Some(syn::PathSegment { ref ident, .. }) = segments.last() {
            return Ok(Some(ident.to_string()));
        }
    }
    return Ok(None);
}

// 定义一个用于实现''visit' Trait的结构体， 结构体中定义了一些字段， 用于储存筛选条件以及筛选结果。
struct TypePathVisitor {
    generic_type_names: Vec<String>,
    associated_types: HashMap<String, Vec<syn::TypePath>>,
}

impl<'ast> Visit<'ast> for TypePathVisitor {
    // visit_type_path 这个回调函数就是我们所关心的
    fn visit_type_path(&mut self, node: &'ast syn::TypePath) {
        if node.path.segments.len() >= 2 {
            let generic_type_name = node.path.segments[0].ident.to_string();
            if self.generic_type_names.contains(&generic_type_name) {
                self.associated_types
                    .entry(generic_type_name)
                    .or_insert(vec![])
                    .push(node.clone());
            }
        }
        // Visit 模式要求在当前节点访问完成后， 继续调用默认实现的visit方法， 从而遍历到所有的
        // 必须调用这个函数， 否则遍历到这个节点就不再往更深层次走了。
        visit::visit_type_path(self, node);
    }
}

fn get_generic_associated_types(st: &syn::DeriveInput) -> HashMap<String, Vec<syn::TypePath>> {
    // 首先构建筛选条件
    let origin_generic_param_names: Vec<String> = st.generics.params
        .iter()
        .filter_map(|f| {
            if let syn::GenericParam::Type(ref ty) = f {
                return Some(ty.ident.to_string());
            }
            return None;
        })
        .collect();
    let mut visitor = TypePathVisitor {
        generic_type_names: origin_generic_param_names,
        associated_types: HashMap::new(),
    };
    // 以st语法树节点为起点， 开始visit整个st节点的子节点
    visitor.visit_derive_input(st);
    return visitor.associated_types;
}

fn get_struct_escape_hatch(st: &syn::DeriveInput) -> Option<String> {
    if let Some(inert_attr) = st.attrs.last() {
        if let Ok(syn::Meta::List(syn::MetaList { nested, .. })) = inert_attr.parse_meta() {
            if let Some(syn::NestedMeta::Meta(syn::Meta::NameValue(path_value))) = nested.first() {
                if path_value.path.is_ident("bound") {
                    if let syn::Lit::Str(ref lit) = path_value.lit {
                        return Some(lit.value());
                    }
                }
            }
        }
    }
    None
}