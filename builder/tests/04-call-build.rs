// Generate a `build` method to go from builder to original struct.
//
// This method should require that every one of the fields has been explicitly
// set; it should return an error if a field is missing. The precise error type
// is not important. Consider using Box<dyn Error>, which you can construct
// using the impl From<String> for Box<dyn Error>.
//
//     impl CommandBuilder {
//         pub fn build(&mut self) -> Result<Command, Box<dyn Error>> {
//             ...
//         }
//     }

use derive_builder::Builder;

#[derive(Builder)]
pub struct Command {
    executable: String,
    args: Vec<String>,
    env: Vec<String>,
    current_dir: String,
}
// #[derive(Builder)]
// pub struct CommandBuilder {
//     executable: Option<String>,
//     args: Option<Vec<String>>,
//     env: Option<Vec<String>>,
//     current_dir: Option<String>,
// }
// impl CommandBuilder {
//             pub fn build(&mut self) -> Result<Command, Box<dyn Error>> {
//                 // TODO

//                 if self.executable.is_none() {
//                    return Err(format!("{} field is missing", "executable"));
//                 }
//                 if self.args.is_none() {
//                     return Err(format!("{} field is missing", "args"));
//                 }
//                 if self.env.is_none() {
//                     return Err(format!("{} field is missing", "env"));
//                 }
//                 if self.current_dir.is_none() {
//                     return Err(format!("{} field is missing", "current_dir"));
//                 }
//                 Ok(Command{
//                     executable: self.executable.clone().unwrap,
//                     args: self.args.clone().unwrap,
//                     env: self.env.clone().unwrap(),
//                     current_dir: self.current_dir.clone().unwrap(),
//                 })
//             }
//         }
fn main() {
    let mut builder = Command::builder();
    builder.executable("cargo".to_owned());
    builder.args(vec!["build".to_owned(), "--release".to_owned()]);
    builder.env(vec![]);
    builder.current_dir("..".to_owned());

    let command = builder.build().unwrap();
    assert_eq!(command.executable, "cargo");
}
